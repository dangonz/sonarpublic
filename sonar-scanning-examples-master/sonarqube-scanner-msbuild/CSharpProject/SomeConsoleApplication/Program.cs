﻿using System;
namespace SomeConsoleApplication
{
    public class Program
    {
        static void Main(string[] args)
        {
            var iAmTrue = true;
            if (iAmTrue)
            {
                Console.WriteLine("true");
            }
            else if(iAmTrue)
            {
                Console.WriteLine("false"); ;
            }
            else
            {
                Console.WriteLine("false");
            }

            Console.ReadKey();
        }

        public static bool AlwaysReturnsTrue()
        {
            return true;
        }

        public static bool AlwaysReturnsFalse()
        {
            return false;
        }

        public static object Passthrough(object obj)
        {
            return obj;
        }
    }
}
